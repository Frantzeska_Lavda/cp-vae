### **Conditional Prior VAE (CP-VAE)**

Pytorch implementation of CP-VAE, based on our paper: **"Improving VAE generations of multimodal data through data-dependent conditional priors"**